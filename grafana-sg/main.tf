provider "aws" {
  profile = var.profile
  region  = var.region
}

resource "aws_lambda_function" "grafana-sg" {
  function_name = "grafana-sg"
  handler       = "lambda_function.lambda_handler"
  memory_size   = var.memory_size
  role          = "arn:aws:iam::996656702859:role/service-role/grafana-sg-role-skz109bq"
  runtime       = "python3.7"
  timeout       = var.timeout
}
