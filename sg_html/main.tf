provider "aws" {
  profile = var.profile
  region  = var.region
}

resource "aws_lambda_function" "sg_html" {
  function_name = "sg_html"
  handler       = "lambda_function.main"
  memory_size   = var.memory_size
  role          = "arn:aws:iam::996656702859:role/service-role/sg_html-role-hzm74obp"
  runtime       = "python3.8"
  timeout       = var.timeout
}
