provider "aws" {
  profile = var.profile
  region  = var.region
}

resource "aws_lambda_function" "push-alarms-to-mongodb" {
  function_name = "push-alarms-to-mongodb"
  handler       = "lambda_function.main"
  memory_size   = var.memory_size
  role          = "arn:aws:iam::996656702859:role/service-role/sg_html-role-hzm74obp"
  runtime       = "python3.8"
  timeout       = var.timeout
  vpc_config {
    security_group_ids = [
      "sg-03a4ac383fd405e6f",
    ]
    subnet_ids = [
      "subnet-0aa89c263c1c4b0a7",
    ]
  }
}
