provider "aws" {
  profile = var.profile
  region  = var.region
}

resource "aws_lambda_function" "amplify-login-create-auth-challenge-b5ea1b24" {
  function_name = "amplify-login-create-auth-challenge-b5ea1b24"
  handler       = "index.handler"
  memory_size   = var.memory_size
  role          = "arn:aws:iam::996656702859:role/amplify-login-lambda-b5ea1b24"
  runtime       = "nodejs12.x"
  timeout       = var.timeout
}
