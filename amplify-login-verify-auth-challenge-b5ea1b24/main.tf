provider "aws" {
  profile = var.profile
  region  = var.region
}

resource "aws_lambda_function" "amplify-login-verify-auth-challenge-b5ea1b24" {
  function_name = "amplify-login-verify-auth-challenge-b5ea1b24"
  handler       = "index.handler"
  role          = "arn:aws:iam::996656702859:role/amplify-login-lambda-b5ea1b24"
  runtime       = "nodejs12.x"
  memory_size   = var.memory_size
  timeout       = var.timeout
  environment {
    variables = {
      "ENDPOINT" = "https://9ardnongjf.execute-api.us-east-1.amazonaws.com/prod"
    }
  }
}
