provider "aws" {
  profile = var.profile
  region  = var.region
}

resource "aws_lambda_function" "aws-python-rest-api-project-dev-hello" {
  function_name = "aws-python-rest-api-project-dev-hello"
  handler       = "handler.hello"
  memory_size   = 1024
  role          = "arn:aws:iam::996656702859:role/aws-python-rest-api-project-dev-us-east-1-lambdaRole"
  runtime       = "python3.8"
  timeout       = 6
  tags = {
    "STAGE" = "dev"
  }
}
