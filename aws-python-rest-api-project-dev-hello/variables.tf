variable "profile" {
  default     = "default"
  type        = string
  description = "AWS Profile"
}

variable "region" {
  default     = "us-east-1"
  type        = string
  description = "Define the AWS Region"
}

variable "memory_size" {
  default     = 1024
  type        = number
  description = "Define the memory size for the lambda function"
}

variable "timeout" {
  default     = 6
  type        = number
  description = "Define the timeout for the lambda function in seconds"
}
