provider "aws" {
  profile = var.profile
  region  = var.region
}

resource "aws_lambda_function" "security_group_alerts" {
  function_name = "security_group_alerts"
  handler       = "lambda_function.lambda_handler"
  memory_size   = var.memory_size
  role          = "arn:aws:iam::996656702859:role/service-role/sg_html-role-hzm74obp"
  runtime       = "python3.8"
  timeout       = var.timeout
}
