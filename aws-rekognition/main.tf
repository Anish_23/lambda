provider "aws" {
  profile = var.profile
  region  = var.region
}

resource "aws_lambda_function" "aws-rekognition" {
  function_name = "aws-rekognition"
  handler       = "com.cloud.rekognition.lambda.LambdaFunctionHandler::handleRequest"
  memory_size   = var.memory_size
  role          = "arn:aws:iam::996656702859:role/lambda_execution"
  runtime       = "java8"
  timeout       = 300
}
