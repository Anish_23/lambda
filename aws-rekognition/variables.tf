variable "profile" {
  default     = "default"
  type        = string
  description = "AWS Profile"
}

variable "region" {
  default     = "us-east-1"
  type        = string
  description = "Define the AWS Region"
}

variable "memory_size" {
  default     = 512
  type        = number
  description = "Define the memory size for the lambda function"
}

variable "timeout" {
  default     = 300
  type        = number
  description = "Define the timeout for the lambda function in seconds"
}
